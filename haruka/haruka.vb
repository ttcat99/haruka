﻿Public Class haruka

    'FTP接続情報
    Public FTPServerName As String          'ディレクトリパスまで（最後のスラッシュ含まない）
    Public FTPUserName As String
    Public FTPPassword As String

    'FTP転送内容
    Public SourceFileName As String
    Public RemoteFileName As String
    Public IsBinary As Boolean = True       'デフォルトでバイナリモード
    Public IsPASV As Boolean = False

    Public Sub New(ByVal Fname As String)    'コンストラクタ。ファイル名を要求する
        SourceFileName = Fname
    End Sub

    Public Sub FileUpload()
        Dim u As New Uri("ftp://" & FTPServerName & "/" & RemoteFileName)

        'FtpWebRequestの作成
        Dim ftpReq As System.Net.FtpWebRequest = CType(System.Net.WebRequest.Create(u), System.Net.FtpWebRequest)
        With ftpReq
            .Credentials = New System.Net.NetworkCredential(FTPUserName, FTPPassword)
            .Method = System.Net.WebRequestMethods.Ftp.UploadFile
            .KeepAlive = False
            .UseBinary = IsBinary
            .UsePassive = IsPASV
        End With

        Dim reqStrm As System.IO.Stream = ftpReq.GetRequestStream()
        Dim fs As New System.IO.FileStream(SourceFileName, System.IO.FileMode.Open, System.IO.FileAccess.Read)
        Dim buffer(1023) As Byte
        While True
            Dim readSize As Integer = fs.Read(buffer, 0, buffer.Length)
            If readSize = 0 Then
                Exit While
            End If
            reqStrm.Write(buffer, 0, readSize)
        End While
        fs.Close()
        reqStrm.Close()

        Dim ftpRes As System.Net.FtpWebResponse = CType(ftpReq.GetResponse(), System.Net.FtpWebResponse)

        ftpRes.Close()

    End Sub
End Class
